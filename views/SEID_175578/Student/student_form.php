<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Student Grading System</title>
  </head>
  <link rel="stylesheet" href="../../../resources/bootstrap-3.3.7-dist/css/bootstrap.min.css">
  <style media="screen">
    input[type="text"] {
      width: 100%;
      display: block;
      padding: 5px;

    }

    input[type="number"] {
      width: 100%;
      display: block;
      padding: 5px;
    }

    input[type="submit"] {
      width: 20%;
      padding: 15px;
      margin-bottom: 20px;
      border-radius: 10px;
      font-weight: bold;

    }
    .tim{
      background: green;
      width: 960px;
      margin: 0 auto;
      overflow: hidden;
      background-image: url("../../../resources/images/b.jpg");
      margin-top: 40px;
      background-size: 100% 100%;

    }
    .btn:hover{
      color: black;
      background-color: red;
    }
    .page-header{
      color: orangered;
      font-weight: bold;
    }
    .pull-left{
      color:crimson;
    }
    .pull-right{
      color: #761c19;
    }




  </style>
  <body>
    <div class="container tim">
      <h1 class="page-header text-center">Student Grading System</h1>
      <form class="col-md-12" action="process.php" method="post">
        <div class="pull-left col-md-5">
          <label style="font-size:30px;">Personal Information</label><br>
          <label>Student ID: </label><br><br>
          <input type="text" name="studentId" required placeholder="Enter Student ID"><br>
          <label>Name: </label><br><br>
          <input type="text" name="name" required placeholder="Enter Name"><br>
          <label>Date of Birth: </label><br><br>
          <input type="date" name="dob" required><br><br>
          <label>Gender: </label><br><br>
          <input type="radio" name="gender" required value="Male">&nbsp;Male<br>
          <input type="radio" name="gender" required value="Female">&nbsp;Female<br><br>
        </div>
        <div class="pull-right col-md-5">
          <label for="Information" style="font-size:30px;">Mark Information</label><br><br>
          <label>Bangla Mark: </label><br><br>
          <input type="number" name="banglaMark" required placeholder="Enter Mark"><br>

          <label>English Mark: </label><br><br>
          <input type="number" name="englishMark" required placeholder="Enter Mark"><br>

          <label>Math Mark: </label><br><br>
          <input type="number" name="mathMark" required placeholder="Enter Mark "><br>

          <label>ICT Mark: </label><br><br>
          <input type="number" name="ictMark" required placeholder="Enter Mark"><br><br>
        </div>
        <div class="col-md-12">
          <input class="btn btn-success center-block " type="submit" name="submit" value="Submit">
        </div>
      </form>

    </div>
  </body>
</html>
